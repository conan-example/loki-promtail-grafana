# test promtail config
## install promtail (Mac arm64)
```shell
curl -L -o promtail.zip https://github.com/grafana/loki/releases/download/v2.5.0/promtail-darwin-arm64.zip
unzip promtail.zip
rm promtail.zip
sudo mv promtail-darwin-arm64 /usr/local/bin/promtail
```

## run testing command
```shell
cat test.log | promtail --stdin --dry-run -config.file=config.yaml
```
