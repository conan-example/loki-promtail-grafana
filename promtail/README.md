# promtail

## install
```shell
helm -n loki upgrade --install promtail grafana/promtail -f values.yaml --version 5.1.0
```
## test the config
[./test_connfig](test_config/)