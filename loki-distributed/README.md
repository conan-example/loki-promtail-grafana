# Loki
The helm chart used:: https://github.com/grafana/helm-charts/tree/main/charts/loki-distributed  
This is a example of deploy Loki microservices on GKE


## create namespace
```shell
kubectl create ns loki
```

## perpare GCS credential
```shell
kubectl -n loki create secret generic gcs-credentials --from-file=key.json=A_SERVICE_ACCOUNT_KEY_WITH_GCS_IAM_ROLE.json
```

## install via helm
add repo:
```shell
helm repo add grafana https://grafana.github.io/helm-charts
```

install/upgrade:
```shell
helm upgrade -n loki --install loki grafana/loki-distributed --version 0.48.4 -f values.yaml
```

## config
TODO: ref:

## apply HPA
Because the helm chart did not provide behavior setting of HPA, so let's apply if separately
```shell
kubectl -n loki apply -f querier-hpa.yaml
```

