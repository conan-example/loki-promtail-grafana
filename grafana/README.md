# grafana

## create namespace
```shell
kubectl create ns grafana
```

## install
安裝
```shell
helm upgrade --install -n loki grafana grafana/grafana --version 6.29.6 -f values.yaml
```

